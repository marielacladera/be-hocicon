import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const databaseConfig: TypeOrmModuleOptions = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    password: 'mariela',
    username: 'postgres',
    entities: ["dist/**/*.entity.js"],
    database: 'hocicon',
    synchronize: true
}