
<p  align="center">

<a  href="http://nestjs.com/"  target="blank"><img  src="https://nestjs.com/img/logo-small.svg"  width="200"  alt="Nest Logo"  /></a>

</p>

  

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456

[circleci-url]: https://circleci.com/gh/nestjs/nest

## Description

  

Este proyecto consta de 5 endpoints para la gestion de noticias.
1. Crear noticias
2. Leer una noticia por su id
3. Eliminar una noticia
4. Actualizar una noticia
5. Buscar noticias usando un valor (contenido) y el parametro por el cual se quiere buscar
6. Buscar noticias usando los valores anteriores y tambien el orden.
7. Buscar noticias usando el tipo de noticia