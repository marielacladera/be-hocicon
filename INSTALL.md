
<p  align="center">

<a  href="http://nestjs.com/"  target="blank"><img  src="https://nestjs.com/img/logo-small.svg"  width="200"  alt="Nest Logo"  /></a>

</p>

  

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456

[circleci-url]: https://circleci.com/gh/nestjs/nest

## Instalation
1. Ejecutar el comando **npm install --no-package-lock**
2. Abrir pgAdmin crear una base de datos llamado hocicon
3. Modificar el archivo **enviroment.ts** de la carpeta env las password, username o puerto de
acuerdo a lo que tiene configurado en su pc o laptop.
4. Modificar el archivo **data-source.ts** que se encuentra en la carpeta src, de las option el password, 
username o puerto de acuerdo a lo que tiene configurado en su pc o laptop.
5. Ejecutar el comando **npm run seed:run** para registrar 10 datos aleatorios.
6. Ejecutar el comando **npm run start:dev** para levantar el proyecto