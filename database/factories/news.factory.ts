import { News } from "../../src/entities/news.entity";
import { TypeEnum } from "../../src/enums/type.enum";
import { setSeederFactory } from "typeorm-extension";

export default setSeederFactory(News, (faker) => {
    const instance: News = new News();
    
    instance.author = faker.person.fullName();
    instance.content = faker.lorem.paragraph();
    instance.place = faker.location.city();
    instance.postDate = faker.date.anytime();
    instance.title = faker.internet.domainWord();
    instance.type = faker.helpers.arrayElement([TypeEnum.CULTURE, TypeEnum.POLITIC, TypeEnum.SCIENCE]);

    return instance;
});