import { News } from "../../src/entities/news.entity";
import { TypeEnum } from "../../src/enums/type.enum";
import { DataSource, Repository } from "typeorm";
import { Seeder, SeederFactoryManager } from "typeorm-extension";

export default class NewsSeeder implements Seeder {
    public async run(dataSource: DataSource, factoryManager: SeederFactoryManager): Promise<any> {
        const repository: Repository<News> = dataSource.getRepository(News);
        const data: News = {
            title: 'Test',
            author: 'Mariela',
            content: 'Content Test',
            postDate: new Date(),
            type: TypeEnum.CULTURE,
            place: 'Cochabamba',
            id: 1
        }
        const news: News | null = await repository.findOneBy({ title: 'Test'});
        
        if (!news) {
            await repository.insert(data);
        }

        const newsFactory = await factoryManager.get(News);
        await newsFactory.saveMany(10);
    }
}