import { DataSource } from "typeorm";
import { Seeder, SeederFactoryManager, runSeeders } from "typeorm-extension";
import NewsSeeder from "./news.seeder";
import newsFactory from "../factories/news.factory";

export default class InitSeeder implements Seeder {
    public async run(dataSource: DataSource, factoryManager: SeederFactoryManager): Promise<any> {
        await runSeeders(dataSource, {
            seeds: [NewsSeeder],
            factories: [newsFactory]
        });
    }
}