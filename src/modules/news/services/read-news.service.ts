import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { News } from "src/entities/news.entity";
import { Repository } from "typeorm";

@Injectable()
export class ReadNewsService {
    constructor(@InjectRepository(News) private _newsRepository: Repository<News>) {
        
    }

    public async read(newsId: number): Promise<News> {
        
        const newsFounded: News | null = await this._newsRepository.findOne({where: { id: newsId}})
        if (!newsFounded) {
            throw new NotFoundException(`Not found with the id ${newsId}`);
        }

        return this._newsRepository.findOne({where: { id: newsId}});
    }
}