import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { News } from "src/entities/news.entity";
import { Repository } from "typeorm";
import { CreateNewsInput } from "../../../input/create-news.input";

@Injectable()
export class UpdateNewsService {
    constructor(@InjectRepository(News) private _newsRepository: Repository<News>) {
    }

    public async update(newsId: number, newsInput: CreateNewsInput): Promise<News> {
        const newsFounded: News | null = await this._newsRepository.findOne({where: { id: newsId}})
        if (!newsFounded) {
            throw new NotFoundException(`Not found with the id ${newsId}`);
        }

        const newsToUpdate: News = await this._newsRepository.findOne({ where : { id: newsId} });
        const newsUpdated: News = Object.assign(newsToUpdate, newsInput);

        return this._newsRepository.save(newsUpdated);
    }
}