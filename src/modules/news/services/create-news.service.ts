import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { News } from "src/entities/news.entity";
import { Repository } from "typeorm";
import { CreateNewsInput } from "../../../input/create-news.input";

@Injectable()
export class CreateNewsService {
    constructor(@InjectRepository(News) private _newsRepository: Repository<News>) {
        
    }

    public createNews(news: CreateNewsInput): Promise<News> {
        return this._newsRepository.save(news);
    }
}