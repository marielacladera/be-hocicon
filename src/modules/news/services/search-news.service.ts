import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { News } from "../../../entities/news.entity";
import { OrderEnum } from "src/enums/order.enum";
import { TypeEnum } from "src/enums/type.enum";

@Injectable()
export class SearchNewsService {
    constructor(@InjectRepository(News) private _newsRepository: Repository<News>) {

    }

    public search(order: OrderEnum, param: string, criteria?: string, type?: TypeEnum): Promise<News[]> {
        if (criteria) {
            return this._newsRepository
            .createQueryBuilder("news")
            .where(`news.${param} like :criteria`, { criteria: `%${criteria}%` })
            .orderBy(`news.${param}`, order)
            .getMany();
        }

        if (type) {
            return this._newsRepository
            .createQueryBuilder("news")
            .where(`news.type = :type`, { type: `${type}` })
            .orderBy(`news.type`, order)
            .getMany();
        }

        return this._newsRepository
            .createQueryBuilder("news")
            .orderBy(`news.${param}`, order)
            .getMany();
    }
}