import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { News } from "src/entities/news.entity";
import { DeleteResult, Repository } from "typeorm";

@Injectable()
export class DeleteNewsService {
    constructor(@InjectRepository(News) private _newsRepository: Repository<News>) {
        
    }

    public async delete(newsId: number): Promise<DeleteResult> {
        const newsFounded: News | null = await this._newsRepository.findOne({where: { id: newsId}})
        if (!newsFounded) {
            throw new NotFoundException(`Not found with the id ${newsId}`);
        }
        
        return await this._newsRepository.delete({ id: newsId });
    }
}