import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { News } from "../../entities/news.entity";
import { CreateNewsService } from "./services/create-news.service";
import { CreateNewsController } from "./controllers/create-news.controller";
import { ReadNewsController } from "./controllers/read-news.controller";
import { ReadNewsService } from "./services/read-news.service";
import { DeleteNewsService } from "./services/delete-news.service";
import { DeleteNewsController } from "./controllers/delete-news.controller";
import { UpdateNewsController } from "./controllers/update-news.controller";
import { UpdateNewsService } from "./services/update-news.service";
import { SearchNewsController } from "./controllers/search-news.controller";
import { SearchNewsService } from "./services/search-news.service";

@Module({
    imports: [
        TypeOrmModule.forFeature([News]),
    ], 
    providers: [
        CreateNewsService, 
        DeleteNewsService,
        ReadNewsService,
        UpdateNewsService,
        SearchNewsService
    ], 
    controllers: [
        SearchNewsController,
        CreateNewsController, 
        DeleteNewsController,
        ReadNewsController,
        UpdateNewsController
    ]
})
export class NewsModule {
    
}