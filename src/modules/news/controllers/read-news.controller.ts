import { Controller, Get, Param, ParseIntPipe } from "@nestjs/common";
import { Constants } from "src/constants/constants";
import { NewsResponse } from "../../../response/news.response";
import { ReadNewsService } from "../services/read-news.service";

@Controller(Constants.NEWS_PATH)
export class ReadNewsController {
    
    constructor(private _readNewsService: ReadNewsService) {

    }

    @Get(':newsId')
    public read(@Param('newsId', ParseIntPipe) newsId: number): Promise<NewsResponse> {
        return this._readNewsService.read(newsId);     
    }
}