import { Controller, Get, Query } from "@nestjs/common";
import { Constants } from "../../../constants/constants";
import { SearchNewsService } from "../services/search-news.service";
import { News } from "../../../entities/news.entity";
import { OrderEnum } from "src/enums/order.enum";
import { TypeEnum } from "src/enums/type.enum";

@Controller(Constants.SEARCH_PATH)
export class SearchNewsController {
    
    constructor(private _searchNewsService: SearchNewsService) {

    }

    @Get()
    public search(@Query('criteria') criteria: string, @Query('param') param: string, @Query('order') order: OrderEnum, @Query('type') type: TypeEnum): Promise<News[]> {
        return this._searchNewsService.search(order, param, criteria, type);
    }
}