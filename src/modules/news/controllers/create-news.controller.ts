import { Body, Controller, Post } from "@nestjs/common";
import { Constants } from "src/constants/constants";
import { CreateNewsInput } from "src/input/create-news.input";
import { CreateNewsService } from "../services/create-news.service";
import { NewsResponse } from "../../../response/news.response";

@Controller(Constants.NEWS_PATH)
export class CreateNewsController {
    
    constructor(private _createNewsService: CreateNewsService) {

    }

    @Post()
    public create(@Body() newsInput: CreateNewsInput): Promise<NewsResponse> {
        return this._createNewsService.createNews(newsInput);     
    }
}