import { Body, Controller, Get, Param, ParseIntPipe, Put } from "@nestjs/common";
import { Constants } from "src/constants/constants";
import { NewsResponse } from "../../../response/news.response";
import { CreateNewsInput } from "../../../input/create-news.input";
import { UpdateNewsService } from "../services/update-news.service";

@Controller(Constants.NEWS_PATH)
export class UpdateNewsController {
    
    constructor(private _updateNewsService: UpdateNewsService) {

    }

    @Put(':newsId')
    public update(@Param('newsId', ParseIntPipe) newsId: number, @Body() newsInput: CreateNewsInput): Promise<NewsResponse> {
        return this._updateNewsService.update(newsId, newsInput);     
    }
}