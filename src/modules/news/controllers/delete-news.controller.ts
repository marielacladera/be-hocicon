import { Controller, Delete, Param, ParseIntPipe } from "@nestjs/common";
import { Constants } from "src/constants/constants";
import { DeleteNewsService } from "../services/delete-news.service";
import { DeleteResult } from "typeorm";

@Controller(Constants.NEWS_PATH)
export class DeleteNewsController {
    
    constructor(private _deleteNewsService: DeleteNewsService) {

    }

    @Delete(':newsId')
    public delete(@Param('newsId', ParseIntPipe) newsId: number): Promise<DeleteResult> {
        return this._deleteNewsService.delete(newsId);     
    }
}