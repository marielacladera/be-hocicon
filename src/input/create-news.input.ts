import { TypeEnum } from "src/enums/type.enum";

export interface CreateNewsInput {
    author: string;
    content: string;
    place: string;
    postDate: Date;
    title: string;
    type: TypeEnum;
}