import { TypeEnum } from "../enums/type.enum";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class News {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public title: string;

    @Column()
    public postDate: Date;

    @Column()
    public author: string;

    @Column()
    public place: string;

    @Column()
    public content: string;

    @Column()
    public type: TypeEnum;
}