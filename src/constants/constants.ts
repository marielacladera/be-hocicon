export class Constants {
    public static readonly NEWS_PATH: string = '/news'

    public static readonly SEARCH_PATH: string = this.NEWS_PATH + '/search';
}