import InitSeeder from '../database/seeds/init.seeder';
import { DataSource, DataSourceOptions } from 'typeorm';
import { SeederOptions } from 'typeorm-extension';
import { News } from './entities/news.entity';

const options: DataSourceOptions & SeederOptions = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    database: 'hocicon',
    password: 'mariela',
    entities: ["dist/**/*.entity.js", News],
    username: 'postgres',
    seeds: [InitSeeder]
};

export const dataSource = new DataSource(
    options as DataSourceOptions & SeederOptions
);