import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { databaseConfig } from 'env/environment';
import { NewsModule } from './modules/news/news.module';

@Module({
  imports: [
    NewsModule,
    TypeOrmModule.forRoot(databaseConfig)
  ]
})
export class AppModule {}
